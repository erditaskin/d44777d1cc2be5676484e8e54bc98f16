import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Init from "../Init.vue";
import { ReservationStep, ReservationStepRouter } from "@/models/enums"

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Init",
    component: Init
  } 
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

console.log(router);
router.beforeEach((to, from, next) => { 
  if (to.name !== 'Init') {
    next({ name: 'Init' });
  } else {
    next();
  }
});

export default router;
