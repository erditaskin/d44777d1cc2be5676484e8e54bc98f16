export enum ReservationStep {
    SelectHotelAndDate = 1,
    SelectRoomAndView = 2,
    PreviewAndPayment = 3
}

export enum ReservationStepIcon {
    SelectHotelAndDate = 'calendar-alt',
    SelectRoomAndView = 'bed',
    PreviewAndPayment = 'credit-card'
}

export enum ReservationStepRouter {
    SelectHotelAndDate = 'SelectHotelAndDate',
    SelectRoomAndView = 'SelectRoomAndView',
    PreviewAndPayment = 'PreviewAndPayment'
}

