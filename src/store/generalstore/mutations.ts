import store from '@/store/index'
import * as consts from '@/models/consts/params'
import { ReservationStep } from '@/models/enums'

export default { 
    initialState(state:any){
        const reservationStep = localStorage.getItem(consts.LOC_STORE_RESERVATION_STEP);
        if (reservationStep != null && reservationStep in ReservationStep) {
            state.reservationStep = reservationStep;
        }
        else state.reservationStep = ReservationStep.SelectHotelAndDate;
        state.reservationSteps = [];
    },
    setReservationSteps(state: any, reservationSteps:any[]) {
        state.reservationSteps = reservationSteps;
    },
    setReservationProgress(state: any, reservationStep: ReservationStep) {
        state.reservationStep = reservationStep;
        localStorage.setItem(consts.LOC_STORE_RESERVATION_STEP, reservationStep.toString());
    }
}

