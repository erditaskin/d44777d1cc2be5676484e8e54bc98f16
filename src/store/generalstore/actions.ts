import { ReservationStep } from "@/models/enums"


export default{
    initialState(context: any) {
        context.commit('initialState')
    },
    setReservationSteps(context:any, reservationSteps:any[]){
        context.commit('setReservationSteps', reservationSteps)
    },
    setReservationProgress(context:any, reservationStep:ReservationStep){
        context.commit('setReservationProgress', reservationStep)
    }
}