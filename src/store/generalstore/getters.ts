import store from '@/store/index'
import * as consts from '@/models/consts/params'

export default{
    getReservationSteps(state: any) {
        return state.reservationSteps;
    },
    getReservationProgress(state: any) {
        return state.reservationStep;
    }
}