import Vue from "vue";
import Vuex from "vuex";

import generalStoreModule from "./generalstore"

Vue.use(Vuex);
Vue.config.devtools = true;

export default new Vuex.Store({
  modules: {
    generalStore: generalStoreModule
  }
});
